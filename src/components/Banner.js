import Carousel from 'react-bootstrap/Carousel';
import Slider1 from "../images/slider1.png";
import Slider2 from "../images/slider2.png";
import Slider3 from "../images/slider3.png";

export default function Banner() {
  return (
    <Carousel className="carousel-banner mt-1">
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={Slider1}
          alt="First slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={Slider2}
          alt="Second slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={Slider3}
          alt="Third slide"
        />
      </Carousel.Item>
    </Carousel>
  );
}
