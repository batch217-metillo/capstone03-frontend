import {Carousel, Row, Col, Card, Container, Button} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Necklace from "../images/productCardNecklace.png";
import Pendant from "../images/productCardPendant.png";
import Earrings from "../images/productCardEarrings.png";
import Ring from "../images/productCardRing.png";
import Watch from "../images/productCardWatch.png";
import Bracelet from "../images/productCardBracelet.png";
import Cufflink from "../images/productCardCufflink.png";
import Money from "../images/productCardMoney.png";

export default function ProductCategoryCard() {
	return (
	<>
		<h1 className="our-products"> Product Category </h1>
				<Carousel className="carousel">
					{/*FIRST CARD*/}
				      <Carousel.Item className="carousel-item1">
				      	<Row className="my-4">
					        <Col xs={6} md={3}>
					          <Card className="productCards1 p-3">
					            <Card.Body>
					            <Card.Img variant="top" src={Necklace} />
					              <Card.Title>
					                <h3 className="carousel-headers1a"><strong>Necklace</strong></h3>
					                <h4 className="carousel-headers2a"><strong>Collections</strong></h4>
					              </Card.Title>
					              <Container className="p-1 text-center">
					              <Button className="buttonA text-center" as={Link} to={`/products`} variant="secondary">Explore All &#8594;</Button>
					              </Container>
					            </Card.Body>
					          </Card>
					        </Col>

					        <Col xs={6} md={3}>
					          <Card className="productCards2 p-3">
					            <Card.Body>
					            <Card.Img variant="top" src={Pendant} />
					              <Card.Title>
					                <h3 className="carousel-headers1b"><strong>Pendant</strong></h3>
					                <h4 className="carousel-headers2b"><strong>Collections</strong></h4>
					              </Card.Title>
					              <Container className="p-1 text-center">
					              <Button className="text-center" as={Link} to={`/products`} variant="dark">Explore All &#8594;</Button>
					              </Container>
					            </Card.Body>
					          </Card>
					        </Col>

					        <Col xs={6} md={3}>
					          <Card className="productCards1 p-3">
					            <Card.Body>
					            <Card.Img variant="top" src={Earrings} />
					              <Card.Title>
					                <h3 className="carousel-headers1a"><strong>Earrings</strong></h3>
					                <h4 className="carousel-headers2a"><strong>Collections</strong></h4>
					              </Card.Title>
					              <Container className="p-1 text-center">
					              <Button className="buttonA text-center" as={Link} to={`/products`} variant="secondary">Explore All &#8594;</Button>
					              </Container>
					            </Card.Body>
					          </Card>
					        </Col>

					        <Col xs={6} md={3}>
					          <Card className="productCards2 p-3">
					            <Card.Body>
					            <Card.Img variant="top" src={Ring} />
					              <Card.Title>
					                <h3 className="carousel-headers1b"><strong>Ring</strong></h3>
					                <h4 className="carousel-headers2b"><strong>Collections</strong></h4>
					              </Card.Title>
					              <Container className="p-1 text-center">
					              <Button className="text-center" as={Link} to={`/products`} variant="dark">Explore All &#8594;</Button>
					              </Container>
					            </Card.Body>
					          </Card>
					        </Col>
					    </Row>
				      </Carousel.Item>

				      {/*SECOND CARD*/}
				      <Carousel.Item className="carousel-item2">
				      	      	<Row className="my-4">
				      		        <Col xs={6} md={3}>
				      		          <Card className="productCards1 p-3">
				      		            <Card.Body>
				      		            <Card.Img variant="top" src={Watch} />
				      		              <Card.Title>
				      		                <h3 className="carousel-headers1a"><strong>Watch</strong></h3>
				      		                <h4 className="carousel-headers2a"><strong>Collections</strong></h4>
				      		              </Card.Title>
				      		              <Container className="p-1 text-center">
				      		              <Button className="buttonA text-center" as={Link} to={`/products`} variant="dark">Explore All &#8594;</Button>
				      		              </Container>
				      		            </Card.Body>
				      		          </Card>
				      		        </Col>

				      		        <Col xs={6} md={3}>
				      		          <Card className="productCards2 p-3">
				      		            <Card.Body>
				      		            <Card.Img variant="top" src={Bracelet} />
				      		              <Card.Title>
				      		                <h3 className="carousel-headers1b"><strong>Bracelet</strong></h3>
				      		                <h4 className="carousel-headers2b"><strong>Collections</strong></h4>
				      		              </Card.Title>
				      		              <Container className="p-1 text-center">
				      		              <Button className="text-center" as={Link} to={`/products`} variant="dark">Explore All &#8594;</Button>
				      		              </Container>
				      		            </Card.Body>
				      		          </Card>
				      		        </Col>

				      		        <Col xs={6} md={3}>
				      		          <Card className="productCards1 p-3">
				      		            <Card.Body>
				      		            <Card.Img variant="top" src={Cufflink} />
				      		              <Card.Title>
				      		                <h3 className="carousel-headers1a"><strong>Cufflink</strong></h3>
				      		                <h4 className="carousel-headers2a"><strong>Collections</strong></h4>
				      		              </Card.Title>
				      		              <Container className="p-1 text-center">
				      		              <Button className="buttonA text-center" as={Link} to={`/products`} variant="dark">Explore All &#8594;</Button>
				      		              </Container>
				      		            </Card.Body>
				      		          </Card>
				      		        </Col>

				      		        <Col xs={6} md={3}>
				      		          <Card className="productCards2 p-3">
				      		            <Card.Body>
				      		            <Card.Img variant="top" src={Money} />
				      		              <Card.Title>
				      		                <h3 className="carousel-headers1b"><strong>Money Bar</strong></h3>
				      		                <h4 className="carousel-headers2b"><strong>Collections</strong></h4>
				      		              </Card.Title>
				      		              <Container className="p-1 text-center">
				      		              <Button className="text-center" as={Link} to={`/products`} variant="dark">Explore All &#8594;</Button>
				      		              </Container>
				      		            </Card.Body>
				      		          </Card>
				      		        </Col>
				      		    </Row>
				      </Carousel.Item>
				    </Carousel>
    </>
    )
}