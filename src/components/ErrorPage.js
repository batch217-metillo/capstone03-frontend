import { Container, Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import {useContext} from "react";
import Image from "../images/error404.gif";
import UserContext from "../UserContext";

export default function ErrorPage(){

const {user} = useContext(UserContext);

	return (
		<>
      <Container fluid>
          <Row >
              <Col className="pt-3">
                      <img
                            className="w-100"
                            src={Image}
                            alt="error image"
                          />
              </Col>
              <Col className="pt-3 mt-5">
                      <h1 className="pt-4 mt-5">ERROR 404</h1>
                      <h3>Page Not Found!</h3>
                      <p>Sorry but the page you're looking for doesn't exist!</p>
                      {
                          (user.isAdmin)
                          ?
                          <Button as={Link} to="/admin" variant="dark">Admin Dashboard</Button>
                          :
                          <Button as={Link} to="/" variant="dark">Back to Home</Button>
                      }
              </Col>
          </Row>
      </Container>
	</>
	)
}