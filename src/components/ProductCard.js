import {Row, Col, Container, Card, Button} from "react-bootstrap";
import {Link} from "react-router-dom";
import "../App.css";

export default function ProductCard({productProp}) {
 
    const { _id, name, category, description, stocks, price } = productProp;
    console.log(productProp);

    return (
         <Col xs={6} md={4} className="product-item">
               <Card className="prod-card mt-3 mx-2 h-100">
                <Card.Body className="mx-5 my-5">
                 <Card.Title className="fs-4 fw-bold mb-3">
                    {name}
                 </Card.Title>

                 <Card.Subtitle className="fs-5">
                    Category:
                 </Card.Subtitle>
                 <Card.Text className="fs-5">
                    {category}
                 </Card.Text>

                 <Card.Subtitle className="fs-5">
                    Description:
                 </Card.Subtitle>
                 <Card.Text className="fs-5">
                    {description}
                 </Card.Text>

                 <Card.Subtitle className="fs-5">
                    Stocks:
                 </Card.Subtitle>
                 <Card.Text className="fs-5">
                    {stocks} available
                 </Card.Text>
                             
                 <Card.Subtitle className="fs-5">
                    Price:
                 </Card.Subtitle>
                 <Card.Text className="fs-5">
                    ₱ {price}
                 </Card.Text>

                 <Button as={Link} to={`/products/${_id}`} variant="dark">Details</Button>
                </Card.Body>
            </Card>
          </Col>        
    )
}
