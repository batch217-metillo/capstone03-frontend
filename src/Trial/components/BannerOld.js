import React from 'react'
import {Carousel, Container, Row, Col} from 'react-bootstrap';

export default function BannerOld() {
  return (
    <Container fluid>
      <Row >
      {/*FIRST COLUMN*/}
        <Col className="p-1 mt-5">
            <h1 className="display-4 fw-normal" data-aos="zoom-out-down" data-aos-delay="100">Welcome,</h1>
             <h2 className="display-4 fw-normal" data-aos="zoom-out-down" data-aos-delay="100">Au Diggers!</h2>
            <p className="lead fw-normal" data-aos="zoom-out-down" data-aos-delay="200">Everything feels different with gold!</p>
            <a className="btn btn-outline-secondary" href="#products" data-aos="zoom-out-down" data-aos-delay="300">Shop Now!</a>
        </Col>


        {/*SECOND COLUMN*/}
        <Col>
            <Carousel>
                {/*FIRST CAROUSEL ITEM*/}
                  <Carousel.Item>
                    {/*<img
                      className="d-block w-75"
                      src="https://i.pinimg.com/originals/07/75/e8/0775e856ff318dfc4713fb71e6117c52.gif"
                      alt="First slide"
                    />*/}

                    <img 
                    className="col-md-4 d-none d-md-block w-100" 
                    src="https://i.pinimg.com/originals/07/75/e8/0775e856ff318dfc4713fb71e6117c52.gif" 
                    data-aos="zoom-out-down" 
                    data-aos-delay="700" 
                    alt="mario" />

                    <Carousel.Caption>
                      <h3>First slide label</h3>
                      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                  </Carousel.Item>


                {/*SECOND CAROUSEL ITEM*/}
                  <Carousel.Item>
                    {/*<img
                      className="d-block w-75"
                      src="https://i.pinimg.com/originals/07/75/e8/0775e856ff318dfc4713fb71e6117c52.gif"
                      alt="Second slide"
                    />*/}

                    <img 
                    className="col-md-4 d-none d-md-block w-100" 
                    src="https://i.pinimg.com/originals/07/75/e8/0775e856ff318dfc4713fb71e6117c52.gif" 
                    data-aos="zoom-out-down" 
                    data-aos-delay="700" 
                    alt="mario" />

                    <Carousel.Caption>
                      <h3>Second slide label</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                  </Carousel.Item>

                {/*THIRD CAROUSEL ITEM*/}
                  <Carousel.Item>
                    {/*<img
                      className="d-block w-75"
                      src="https://i.pinimg.com/originals/07/75/e8/0775e856ff318dfc4713fb71e6117c52.gif"
                      alt="Third slide"
                    />*/}

                    <img 
                    className="col-md-4 d-none d-md-block w-100" 
                    src="https://i.pinimg.com/originals/07/75/e8/0775e856ff318dfc4713fb71e6117c52.gif" 
                    data-aos="zoom-out-down" 
                    data-aos-delay="700" 
                    alt="mario" />

                    <Carousel.Caption>
                      <h3>Third slide label</h3>
                      <p>
                        Praesent commodo cursus magna, vel scelerisque nisl consectetur.
                      </p>
                    </Carousel.Caption>
                  </Carousel.Item>
            </Carousel>
        </Col>
      </Row>
    </Container>
  );
}












/*export default function Hero() {
    return (
        <>
            <div className="row mx-auto my-5 d-flex justify-content-around">
                <div className="col-md-5">
                    <h1 className="display-4 fw-normal" data-aos="zoom-out-down" data-aos-delay="100">Welcome,</h1>
                     <h2 className="display-4 fw-normal" data-aos="zoom-out-down" data-aos-delay="100">Au Diggers!</h2>
                    <p className="lead fw-normal" data-aos="zoom-out-down" data-aos-delay="200">Everything feels different with gold!</p>
                    <a className="btn btn-outline-secondary" href="#products" data-aos="zoom-out-down" data-aos-delay="300">Shop Now!</a>
            </div>
                <img className="col-md-4 d-none d-md-block" src="https://i.pinimg.com/originals/07/75/e8/0775e856ff318dfc4713fb71e6117c52.gif" data-aos="zoom-out-down" data-aos-delay="700" alt="mario" />

                <div className="row mx-auto my-5 d-flex justify-content-around">
                <Carousel>
                      <Carousel.Item>
                        <img
                          className="d-block w-100"
                          src="https://i.pinimg.com/originals/07/75/e8/0775e856ff318dfc4713fb71e6117c52.gif"
                          alt="First slide"
                        />
                        <Carousel.Caption>
                          <h3>First slide label</h3>
                          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                        </Carousel.Caption>
                      </Carousel.Item>
                      <Carousel.Item>
                        <img
                          className="d-block w-100"
                          src="https://i.pinimg.com/originals/07/75/e8/0775e856ff318dfc4713fb71e6117c52.gif"
                          alt="Second slide"
                        />

                        <Carousel.Caption>
                          <h3>Second slide label</h3>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </Carousel.Caption>
                      </Carousel.Item>
                      <Carousel.Item>
                        <img
                          className="d-block w-100"
                          src="https://i.pinimg.com/originals/07/75/e8/0775e856ff318dfc4713fb71e6117c52.gif"
                          alt="Third slide"
                        />

                        <Carousel.Caption>
                          <h3>Third slide label</h3>
                          <p>
                            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
                          </p>
                        </Carousel.Caption>
                      </Carousel.Item>
                </Carousel>
                </div>
            </div>

            <div className="row text-center my-5">
                <div className="col-md-4 d-flex flex-column align-items-center justify-content-between" data-aos="zoom-in" data-aos-delay="300">
                    <div className="ratio ratio-1x1 w-50 rounded-circle overflow-hidden">
                        <img src="https://media2.giphy.com/media/E3y79zUo2V4v8AFG2V/giphy.gif" className="img-cover" alt="nft" />
                    </div>
                    <h4 className="fw-normal">NFT Payment</h4>
                    <p>We accept NFT Payments here. It's already 2022 and NFT will be the next big thing.</p>
                    
                </div>
                <div className="col-md-4 d-flex flex-column align-items-center justify-content-between" data-aos="zoom-in" data-aos-delay="600">
                    <div className="ratio ratio-1x1 w-50 rounded-circle overflow-hidden">
                        <img src="https://c.tenor.com/61vG3YfDrUMAAAAC/wreck-it-ralph-disney.gif" className="img-cover" alt="wralph" />
                    </div>
                    <h4 className="fw-normal">Free Consult</h4>
                    <p>Some of our products here are really expensive, so we give consultation for free.</p>
                    
                </div>
                <div className="col-md-4 d-flex flex-column align-items-center justify-content-between" data-aos="zoom-in" data-aos-delay="900">
                    <div className="ratio ratio-1x1 w-50 rounded-circle overflow-hidden">
                        <img src="https://media1.giphy.com/media/pItZ4gr8ZywDAqbc8X/giphy.gif?cid=ecf05e47utu0xn3ugaix90fj75cyfbpp1he2b8odhhhfmjqq&rid=giphy.gif&ct=g" className="img-cover" alt="sale" />
                    </div>
                    <h4 className="fw-normal">Sales and Discounts</h4>
                    <p>Every month we have an exciting discounts and sudden price sales. You don't want to miss that out</p>
                    
                </div>
            </div>
        </>
    )
}
*/