import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import {Row, Col, Card, Container, Button} from 'react-bootstrap';
import Necklace from "../images/productCardNecklace.png";
import Pendant from "../images/productCardPendant.png";
import Earrings from "../images/productCardEarrings.png";
import Ring from "../images/productCardRing.png";

export default function ProductCardOldV2() {
	const responsive = {
	  superLargeDesktop: {
	    // the naming can be any, depends on you.
	    breakpoint: { max: 4000, min: 3000 },
	    items: 4
	  },
	  desktop: {
	    breakpoint: { max: 3000, min: 1024 },
	    items: 3
	  },
	  tablet: {
	    breakpoint: { max: 1024, min: 464 },
	    items: 2
	  },
	  mobile: {
	    breakpoint: { max: 464, min: 0 },
	    items: 1
	  }
	};

	return (
		<>
			<h1 className="our-products"> Product Category </h1>

			<Carousel className="productCards" responsive={responsive}>

			  <div>
			  	  <Card className="productCards p-3">
			  	    <Card.Img variant="top" src={Necklace} />
			  	    <Card.Body>
			  	      <Card.Title>
			  	        <h2>NECKLACES</h2>
			  	      </Card.Title>
			  	      <Card.Text>
			  	        asfgasfljalskfjklasjfkas
			  	      </Card.Text>
			  	      <Button variant="primary">Buy Now!</Button>
			  	    </Card.Body>
			  	  </Card>
			  </div>

			  <div>
			  	  <Card className="productCards p-3">
			  	    <Card.Img variant="top" src={Pendant} />
			  	    <Card.Body>
			  	      <Card.Title>
			  	        <h2>PENDANTS</h2>
			  	      </Card.Title>
			  	      <Card.Text>
			  	        asfgasfljalskfjklasjfkas
			  	      </Card.Text>
			  	    </Card.Body>
			  	  </Card>
			  </div>

			  <div>
			  	  <Card className="productCards p-3">
			  	    <Card.Img variant="top" src={Earrings} />
			  	    <Card.Body>
			  	      <Card.Title>
			  	        <h2>EARRINGS</h2>
			  	      </Card.Title>
			  	      <Card.Text>
			  	        asfgasfljalskfjklasjfkas
			  	      </Card.Text>
			  	    </Card.Body>
			  	  </Card>
			  </div>

			  <div>
			  	  <Card className="productCards p-3">
			  	    <Card.Img variant="top" src={Ring} />
			  	    <Card.Body>
			  	      <Card.Title>
			  	        <h2>RINGS</h2>
			  	      </Card.Title>
			  	      <Card.Text>
			  	        asfgasfljalskfjklasjfkas
			  	      </Card.Text>
			  	    </Card.Body>
			  	  </Card>
			  </div>
			</Carousel>
		</>

	)
}