import React from 'react';
import {Carousel, Container, Row, Col, Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import { useNavigate } from "react-router-dom";
import {Navigate, Link} from 'react-router-dom';
import Swal from "sweetalert2";
import RSlider1 from "../images/Rslider1.png";
import RSlider2 from "../images/Rslider2.png";
import RSlider3 from "../images/Rslider3.png";

export default function RegisterTrial(){

	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const [errors, setErrors] = useState([]);
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [username, setUsername] = useState('');
	const [emailAddress, setEmailAddress] = useState('');
	const [phoneNumber, setPhoneNumber] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    username: "",
    emailAddress: "",
    phoneNumber: "",
    password1: "",
    password2: ""
  });

  function handleChange(event) {
    const { name, value } = event.target;
    setFormData(prevFormData => {
      return {
        ...prevFormData,
        [name]: value
      }
    });
  }

	console.log(firstName);
	console.log(lastName);
	console.log(username);
	console.log(emailAddress);
	console.log(phoneNumber);
	console.log(password1);
	console.log(password2);

	useEffect(() => {

		if (
      formData.firstName.length > 0 &&
      formData.lastName.length > 0 &&
      formData.username.length > 0 &&
      formData.emailAddress.length > 0 &&
      formData.phoneNumber.length > 0 &&
      formData.password1.length > 0 &&
      formData.password2.length > 0 &&
      formData.password1 === formData.password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [formData]);

	function registerUser(e){
	    e.preventDefault();
	    setErrors([]);
	    fetch(`${process.env.REACT_APP_API_URL}/register`, {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        firstName: formData.firstName,
        lastName: formData.lastName,
        username: formData.username,
        emailAddress: formData.emailAddress,
        phoneNumber: formData.phoneNumber,
        password: formData.password1
      })
    })
      .then(response => response.json())
      .then(data => {
        if (data.errors !== undefined) {
          data.errors.map(errors => {
            return setErrors(prevErrors => {
              return [...prevErrors, errors]
            })
          })
        } else if (data.message.indexOf("Email") !== -1) {
          return setErrors([{ message: data.message }]);
        }
        else {
          let timerInterval;
          Swal.fire({
            title: "Registration Successful",
            text: "You will now be redirected to the login page. :>",
            timer: 3000,
            timerProgressBar: true,
            didOpen: () => {
              Swal.showLoading()
              const b = Swal.getHtmlContainer().querySelector('b')
              timerInterval = setInterval(() => {
                b.textContent = Swal.getTimerLeft()
              }, 100)
            },
            willClose: () => {
              clearInterval(timerInterval);
              setFormData({
                fullName: "",
                email: "",
                mobileNo: "",
                password1: "",
                password2: ""
              });
            }
          })
          setTimeout(() => navigate("/login"), 3000);

        }
      })
  }


	return (
		(user.id !== null)
		?
		<Navigate to="/"/>
		:
		<Container fluid>
		      
		      <Row>
		        <Col xs={12} md={6}>
		          <Carousel className="carousel-banner mt-1">
                      {/*FIRST CAROUSEL ITEM*/}
                        <Carousel.Item>
                          <img
                            className="d-block w-100"
                            src={RSlider1}
                            alt="First slide"
                          />
                          <Carousel.Caption>
                            <h3>First slide label</h3>
                            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                          </Carousel.Caption>
                        </Carousel.Item>

                      {/*SECOND CAROUSEL ITEM*/}
                        <Carousel.Item>
                          <img
                            className="d-block w-100"
                            src={RSlider2}
                            alt="Second slide"
                          />
                        </Carousel.Item>

                      {/*THIRD CAROUSEL ITEM*/}
                        <Carousel.Item>
                          <img
                            className="d-block w-100"
                            src={RSlider3}
                            alt="Third slide"
                          />
                        </Carousel.Item>
                      </Carousel>
		        </Col>

		        <Col className="p-2" xs={12} md={6}>

		        <h4 className="text-center"> Welcome to Gold Diggers PH</h4>
		          <Form onSubmit={(e) => registerUser(e)}>
		          	<div className="row"> 
		          	{/*FIRST NAME*/}
		          		<Form.Group className="form-group col-md-6 mb-1" controlId="firstName">
		                  <Form.Label>First Name</Form.Label>
		                  <Form.Control type="text" placeholder="Enter First Name" 
		                  required onChange={e => setFirstName(e.target.value)} value={firstName}/>
		                </Form.Group>

		            {/*LAST NAME*/}
		                <Form.Group className="form-group col-md-6 mb-1" controlId="lastName">
		                  <Form.Label>Last Name</Form.Label>
		                  <Form.Control type="text" placeholder="Enter Last Name" 
		                  required onChange={e => setLastName(e.target.value)} value={lastName}/>
		                </Form.Group>
		            </div>

		            {/*USERNAME*/}
		                <Form.Group className="mb-1" controlId="username">
		                  <Form.Label>Username</Form.Label>
		                  <Form.Control type="text" placeholder="Enter Username" 
		                  required onChange={e => setUsername(e.target.value)} value={username}/>
		                </Form.Group>

		            {/*EMAIL ADDRESS*/}
		                <Form.Group className="mb-1" controlId="emailAddress">
		                  <Form.Label>Email Address</Form.Label>
		                  <Form.Control type="email" placeholder="name@example.com" 
		                  required onChange={e => setEmailAddress(e.target.value)} value={emailAddress}/>
		                  <Form.Text className="text-muted">
		                    We'll never share your email with anyone else.
		                  </Form.Text>
		                </Form.Group>

		            {/*PHONE NUMBER*/}
		                <Form.Group className="mb-1" controlId="phoneNumber">
		                  <Form.Label>Phone Number</Form.Label>
		                  <Form.Control type="text" placeholder="+639xxxxxxxxx" 
		                  required onChange={e => setPhoneNumber(e.target.value)} value={phoneNumber}/>
		                </Form.Group>

		            <div className="row">
		            {/*PASSWORD 1*/}
		                <Form.Group className="form-group col-md-6 mb-1" controlId="password1">
		                  <Form.Label>Password</Form.Label>
		                  <Form.Control type="password" placeholder="Password" 
		                  required onChange={e => setPassword1(e.target.value)} value={password1}/>
		                </Form.Group>

		            {/*PASSWORD 2*/}
		                <Form.Group className="form-group col-md-6 mb-1" controlId="password2">
		                  <Form.Label>Verify Password</Form.Label>
		                  <Form.Control type="password" placeholder="Verify Password" 
		                  required onChange={e => setPassword2(e.target.value)} value={password2}/>
		                </Form.Group>
		                {/*<Form.Group className="mb-3" controlId="formBasicCheckbox">
		                  <Form.Check type="checkbox" label="Check me out" />
		                </Form.Group>*/}
		            </div>

		            {/*SUBMIT BUTTON*/}
	                	{
	                		isActive 
	                		?
	                		<Container className="p-2 text-center">
	                		<Button variant="primary" type="submit" id="submitBtn">
	                		Submit
	                		</Button>
	                		</Container>
	                		:
	                		<Container className="p-2 text-center">
	                		<Button variant="primary" type="submit" id="submitBtn" disabled>
	                		Register
	                		</Button>
	                		</Container>
	                	}
	                 <p className="text-center">Already have an account? <a href="/login">Login here!</a></p>

		                
		          </Form>
		        </Col>
		      </Row>
		</Container>

		
	)
}